<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
	<head>
		<meta charset="utf-8">
		<title>簡易Twitter</title>
	</head>
	<body>
		<div class="main-contents">
			<div class="header">
				<a href="show">ログイン</a>
				<a href="signup">登録する</a>
			</div>
			<div class="copyright"> Copyright(c)NumayamaKozue</div>
		</div>
	</body>
</html>
